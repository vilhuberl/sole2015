README
=====

This repository contains code, data, and output from an effort to visualize the themes in papers and presentations at [SOLE 2015](http://www.sole-jole.org/2015.htm). The initial thought was that this could help organize sessions (we ended up doing it the old-fashioned way, with some help from the word frequencies), but we thought the pictures were pretty enough to warrant (a) inclusion into the program (b) an explanation of how we generated them.

The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2015](https://bitbucket.org/vilhuberl/sole2015), and you can find the browsable page at [http://www.vrdc.cornell.edu/sole2015/](http://www.vrdc.cornell.edu/sole2015/).

[Lars Vilhuber](mailto:lars.vilhuber@cornell.edu), [Cornell University](http://www.cornell.edu) [Economics Department](http://economics.cornell.edu) and [Labor Dynamics Institute](http://www.ilr.cornell.edu/ldi/).

