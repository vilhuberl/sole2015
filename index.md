# SOLE 2015 Posters
Lars Vilhuber  

=============================

These pages are an effort to visualize the themes in posters at [SOLE 2015](http://www.sole-jole.org/2015.html). We did something similar for [SOLE 2014](../sole2014/). 

Key documents
------------

 * [graphing.SOLE2015.posters_only.html](graphing.SOLE2015.posters_only.html): using just the words in titles of posters (Sources: [graphing.SOLE2015.posters_only.Rmd](graphing.SOLE2015.posters_only.Rmd) and its output [graphing.SOLE2015.posters_only.md](graphing.SOLE2015.posters_only.md))
 * a few alternate, high-resolution pictures:
    * [Attempt 1](graphing.SOLE2015.all_titles.HIRES1.png)
    * [Attempt 2](graphing.SOLE2015.all_titles.HIRES2.png)
    * [Attempt 3](graphing.SOLE2015.all_titles.HIRES3.png)
    * [Attempt 4](raphing.SOLE2015.all_titles.png)


Tools
-----
 * [RStudio](http://rstudio.com), a really nice IDE for R and other things
 * [R Markdown](http://rmarkdown.rstudio.com/) ([how to use it in RStudio](http://www.rstudio.com/ide/docs/authoring/using_markdown))
 * [knitr](http://yihui.name/knitr/)
 * R libraries:
    - library([tm](http://cran.r-project.org/web/packages/tm/index.html)) 
    - library([wordcloud](http://cran.r-project.org/web/packages/wordcloud/index.html))

Sources
------
The code behind this endeavor is available at [https://bitbucket.org/vilhuberl/sole2015](https://bitbucket.org/vilhuberl/sole2015), and you can find this page at [http://www.vrdc.cornell.edu/sole2015/](http://www.vrdc.cornell.edu/sole2015/).

Author
-----
[Lars Vilhuber](mailto:lars.vilhuber@cornell.edu), [Cornell University](http://www.cornell.edu) [Economics Department](http://economics.cornell.edu) and [Labor Dynamics Institute](http://www.ilr.cornell.edu/ldi/).

